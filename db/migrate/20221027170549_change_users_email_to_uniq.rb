# frozen_string_literal: true

# Chenage Users email to uniq
class ChangeUsersEmailToUniq < ActiveRecord::Migration[7.0]
  def change
    change_column :users, :email, :string, limit: 254, null: false
    add_index :users, :email, unique: true
  end
end
