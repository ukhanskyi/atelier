# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

# rubocop:disable Rails/Output
print("\nStarts users seeding")
3.times do
  FactoryBot.create(:user)
  print '.'
end
puts("\nUsers are created!")

print("\nStarts posts seeding")
5.times do
  FactoryBot.create(:post)
  print '.'
end
puts("\nPosts are created!")
# rubocop:enable Rails/Output
