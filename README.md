# Atelier README

[![pipeline status](https://gitlab.com/ukhanskyi/atelier/badges/main/pipeline.svg)](https://gitlab.com/ukhanskyi/atelier/-/commits/main)
[![coverage report](https://gitlab.com/ukhanskyi/atelier/badges/main/coverage.svg)](https://gitlab.com/ukhanskyi/atelier/-/commits/main)

## Prerequisites

### To run the project you need:

  [Ruby v.3.1.2 with rbenv](https://github.com/rbenv/rbenv)

  [Rails 7.0.4](https://rubyonrails.org/2022/9/9/Rails-7-0-4-6-1-7-6-0-6-have-been-released)

  PostgreSQL 14.5

  MacOS:
  ```shell
  brew install postgresql@14.5
  ```
  Linux (Ubuntu)
  ```shell
  sudo apt -y install postgresql-14.5
  ```

## Installation of project

  ### Clone the repository

  ```shell
  git clone git@gitlab.com:ukhanskyi/atelier.git
  cd atelier
  ```

  or

  ```shell
  git clone https://gitlab.com/ukhanskyi/atelier.git
  cd atelier
  ```

### Install dependencies

Using [Bundler](https://github.com/bundler/bundler):

```shell
bundle install
```

### Initialize the database

```shell
rails db:create db:migrate db:seed
```

## Server

```shell
rails s
```

## Contributing
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D
