# frozen_string_literal: true

# Routes
Rails.application.routes.draw do
  resources :users
  resources :posts
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root 'posts#index'
  get '/notice', to: 'posts#clear_message'
end
