# frozen_string_literal: true

# Factory for users
FactoryBot.define do
  factory :user do
    name  { Faker::Name.first_name }
    email { Faker::Internet.email }
  end
end
