# frozen_string_literal: true

# Factory for posts
FactoryBot.define do
  factory :post do
    title { Faker::Lorem.sentence }
  end
end
