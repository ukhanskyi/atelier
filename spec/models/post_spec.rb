# frozen_string_literal: true

require 'rails_helper'

RSpec.describe(Post, type: :model) do
  context 'when valid Factory' do
    it 'has a valid factory' do
      expect(FactoryBot.build(:post)).to(be_valid)
    end
  end
end
