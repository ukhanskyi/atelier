# frozen_string_literal: true

require 'rails_helper'

RSpec.describe('posts/index', type: :view) do
  let(:post) { FactoryBot.create(:post, title: 'Title', content: 'Content') }

  before do
    assign(:posts, [post, post])
  end

  it 'renders a list of posts' do
    render
    assert_select 'div>h1', text: post.title.to_s, count: 2
  end
end
