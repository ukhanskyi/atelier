# frozen_string_literal: true

require 'rails_helper'

RSpec.describe('posts/new', type: :view) do
  before do
    # Use build instead of create, to pass a non-saved user
    assign(:post, FactoryBot.build(:post))
  end

  it 'renders new post form' do
    render
    assert_select 'form[action=?][method=?]', posts_path, 'post'
  end
end
