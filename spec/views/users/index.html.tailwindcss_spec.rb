# frozen_string_literal: true

require 'rails_helper'

RSpec.describe('users/index', type: :view) do
  let(:index_user) { FactoryBot.create(:user, name: 'Name', email: 'index_user@example.com') }

  before do
    assign(:users, index_user)
  end

  it 'renders a list of users' do
    render
    assert_select 'div>p', text: index_user.name.to_s, count: 1
  end
end
