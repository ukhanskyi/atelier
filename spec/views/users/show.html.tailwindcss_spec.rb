# frozen_string_literal: true

require 'rails_helper'

RSpec.describe('users/show', type: :view) do
  let(:user) { FactoryBot.create(:user, name: 'Name', email: 'show_user@example.com') }

  before do
    assign(:user, user)
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to(match(/Name/))
  end
end
