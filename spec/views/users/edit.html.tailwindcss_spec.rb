# frozen_string_literal: true

require 'rails_helper'

RSpec.describe('users/edit', type: :view) do
  let(:edit_user) { FactoryBot.create(:user, name: 'Name', email: 'edit_user@example.com') }

  before do
    assign(:user, edit_user)
  end

  it 'renders the edit user form' do
    render

    assert_select 'form[action=?][method=?]', user_path(edit_user), 'post' do
      assert_select 'input[name=?]', 'user[name]'
      assert_select 'input[name=?]', 'user[email]'
    end
  end
end
