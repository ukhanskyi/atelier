# frozen_string_literal: true

# User Model
class User < ApplicationRecord
  validates :name, presence: true, length: 2..50
  validates :email, presence: true, uniqueness: true, length: 5..100,
                    format: { with: URI::MailTo::EMAIL_REGEXP }
end
